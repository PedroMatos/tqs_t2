/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package superkey.keychain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author matos
 */
public class KeyChainTest {
    private KeyChain chainA, emptyChain;
    private KeyEntry entryA;
    private File userKeychainFile;
    private static final String KEYCHAIN_MASTER_KEY = "#wisper"; 
    public KeyChainTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        userKeychainFile = new File("Keychain.txt");
        entryA = new KeyEntry();
        entryA.setApplicationName("appx");
        entryA.setUsername("xx");
        entryA.setPassword("secret@@@");
        try {
            chainA = KeyChain.from(userKeychainFile, new CipherTool(KEYCHAIN_MASTER_KEY));
        } catch (IOException ex) {
            Logger.getLogger(KeyChainTest.class.getName()).log(Level.SEVERE, null, ex);
            fail("exception on the call of Keychain.from");
        }
    }
    @Test
    public void testAdd(){
        chainA.put(entryA);
        assertEquals("failed to add or to return a valid entry", entryA,chainA.find(entryA.key()) );
    }
    @Test 
    public void testFindNonExisting(){
        KeyEntry entry =  chainA.find("x");
        assertEquals("failed to return null when find is called with a non existing key" ,entry,null );
    }
    
    @Test
    public void testAllEntries(){
        boolean found = false;
        chainA.put(entryA);
        Iterable<KeyEntry> keyIterable  = chainA.allEntries();
        for(KeyEntry iteEntry: keyIterable){
            if(iteEntry.equals(entryA)){
                found = true;
                break;
            }
        }
        assertTrue("entry not found on the iterable returned on allEntries",found);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testPutDuplicateKey(){
        chainA.put(entryA);
        chainA.put(entryA);
    }
    @Test
    public void testSave(){
        try {
            chainA = KeyChain.from(new File("TestFile.txt"), new CipherTool("some word"));
            chainA.put(entryA);
            chainA.save();
            chainA = KeyChain.from(new File("TestFile.txt"), new CipherTool("some word"));
    
        } catch (IOException ex) {
            Logger.getLogger(KeyChainTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals("failed to succefully save the keychain",entryA , chainA.find(entryA.key()));
    }
    
    @After
    public void tearDown() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new File("TestFile.txt"));
            writer.print("");
            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(KeyChainTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            writer.close();
        }
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
